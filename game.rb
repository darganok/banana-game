require 'rubygems'
require 'rubygame'

require './lib/gameplay.rb'
require './lib/title.rb'
require './lib/shared.rb'

Rubygame::TTF.setup

class Game

  attr_accessor :screen, :queue, :clock, :state
  def initialize
    @screen = Rubygame::Screen.new [800, 600], 0, [Rubygame::HWSURFACE, Rubygame::DOUBLEBUF]
    @screen.title = 'Banana Man'

    @queue = Rubygame::EventQueue.new

    @clock = Rubygame::Clock.new
    @clock.target_framerate = 60

    @state = nil
    @state_buffer = nil
  end

  def run!
    loop do
      @state.update
      unless @state_buffer == nil
        @state = @state_buffer
        @state_buffer = nil
      end
      @state.draw
      @clock.tick
    end
  end

  def set_state state
    unless @state == nil
      @state_buffer = state
    else
      @state = state
    end
  end

end

g = Game.new
g.set_state Title.new(g)
g.run!
