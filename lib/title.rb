require_relative 'state.rb'
require_relative 'element.rb'
require_relative 'selector.rb'

class Title < State

  def initialize game
    @game = game
    @screen = game.screen
    @queue = game.queue

    @title_text = Text.new 0, 35, "Banana Man", 75
    menu_start = 250
    @new_text = Text.new 0, menu_start, "New Game", 25
    @load_text = Text.new 0, menu_start + 50, "Load Game", 25
    @about_text = Text.new 0, menu_start + 100, "About", 25
    @quit_text = Text.new 0, menu_start + 150, "Quit Game", 25
    @text_list = [@title_text, @new_text, @load_text, @about_text, @quit_text]
    @text_list.each do |t|
      t.center_x @screen.width
    end
    @selector = Selector.new *@text_list[1..@text_list.length]
  end

  def update
    @selector.update
    @queue.each do |ev|
      @selector.handle_event ev
      case ev
      when Rubygame::QuitEvent
        Rubygame.quit
        exit
      when Rubygame::KeyDownEvent
        if ev.key == Rubygame::K_RETURN
          case @selector.choice
          when 0
            #@game.set_state NewGame.new(@game)
            @game.set_state Gameplay.new(@game)
          when 1
            @game.set_state LoadGame.new(@game)
          when 2
            @game.set_state About.new(@game)
          when 3
            Rubygame.quit
            exit
          end
        elsif ev.key == Rubygame::K_ESCAPE
          @queue.push Rubygame::QuitEvent.new
        end
      end
    end
  end

  def draw
    @screen.fill [0, 0, 0]
    @text_list.each do  |t|
      t.draw @screen
    end
    @selector.draw @screen
    @screen.flip
  end

end
