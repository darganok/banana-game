class Text < Element

  def initialize x=0, y=0, text='foobar', size=48
    @font = Rubygame::TTF.new "media/font.ttf", size
    @text = text
    @color = [255, 255, 255]
    super x, y, @font.render(@text, true, @color)
  end

  def rerender
    @width, @height = @font.size_text(@text)
    @surface = @font.render(@text, true, @color)
  end

  def text
    @text
  end

  def text= string
    @text = string
    rerender
  end
end
