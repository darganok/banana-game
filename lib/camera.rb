include Rubygame

class Camera
  attr_accessor :x, :y

  def initialize screen, character, map, background


    @x = 0

    # (0,0) is top left
    @y = map.end_y - screen.height

    @screen = screen
    @character = character
    @map = map
    @background = background

    # create list of game elements to simplify group operations
    @game_elements = [background, map, character]
  end 

  def update
    charx = @character.x - @x
    chary = @character.y - @y

    min_offset_x = min_offset @screen.width
    max_offset_x = max_offset @screen.width

    min_offset_y = min_offset @screen.height
    max_offset_y = max_offset @screen.height

    if charx < min_offset_x && @x > 0
      @x -= min_offset_x - charx
    elsif charx > max_offset_x && @x < @map.end_x - @screen.width
      @x += charx - max_offset_x
    end

    if chary < min_offset_y && @y > 0
      @y -= min_offset_y - chary
    elsif chary > max_offset_y && @y < @map.end_y - @screen.height
      @y += chary - max_offset_y
    end
  end

  def draw
    @screen.fill [0, 0, 0]
    @game_elements.each do |elem|
      elem.draw @screen, @x, @y
    end
    @screen.flip
  end

  def max_offset n
    2*(n/3)
  end

  def min_offset n
    n/3
  end

end
