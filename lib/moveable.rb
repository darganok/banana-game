require_relative 'element.rb'

class Moveable < Element

  attr_accessor :x_speed, :y_speed

  def initialize x, y, x_speed, y_speed, surface
    super x, y, surface
    @x_speed = x_speed
    @y_speed = y_speed
  end

  def move direction
    if direction < 0
      @x -= @x_speed
    elsif direction > 0
      @x += @x_speed
    end
  end
end
