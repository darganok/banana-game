require_relative 'moveable.rb'

class Selector < Moveable

  attr_accessor :choice

  def initialize *texts
    @texts = texts
    @choice = 0
    @x = texts[@choice].x - 10
    @y = texts[@choice].y
    @marker = Rubygame::Surface.new [5, 30]
    @color = [255, 255, 255]
    @marker.fill @color
    super x, y, 10, 10, @marker
  end

  def update
    x_choice = @texts[@choice].x - 10
    y_choice = @texts[@choice].y
    dist_x = [@x_speed, (@x - x_choice).abs].min
    dist_y = [@y_speed, (@y - y_choice).abs].min
    if @x < x_choice
      @x += dist_x
    elsif @x > x_choice
      @x -= dist_x
    end

    if @y < y_choice
      @y += dist_y
    elsif @y > y_choice
      @y -= dist_y
    end
  end

  def draw screen
    @marker.blit screen, [@x, @y]
  end

  def handle_event ev
    case ev
    when Rubygame::KeyDownEvent
      if ev.key == Rubygame::K_UP
        unless @choice == 0
          @choice -= 1
        end
      elsif ev.key == Rubygame::K_DOWN
        unless @choice == @texts.length - 1
          @choice += 1
        end
      end
    end
  end
end
