require_relative 'state.rb'
require_relative 'background.rb'
require_relative 'character.rb'
require_relative 'map.rb'
require_relative 'camera.rb'

class Gameplay < State

  def initialize game
    @game = game
    @screen = game.screen
    @queue = game.queue

    @background = Background.new @screen.width, @screen.height
    @map = Map.new 200, 100
    @character = BananaMan.new 255, 520, @map #@screen.height/2, @screen.width/2
    @camera = Camera.new @screen, @character, @map, @background
  end

  def update
    @character.update
    @queue.each do |event|
      @character.handle_event event
      case event
      when Rubygame::KeyDownEvent
        if event.key == K_ESCAPE
          Rubygame.quit
          exit
        end
      end
    end
    @camera.update
  end

  def draw
    @camera.draw
  end
end
