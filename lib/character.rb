require_relative 'moveable.rb'

class Character < Moveable

  attr_accessor :cur_x_speed, :cur_y_speed, :x_accel, :y_accel

  def initialize x, y, x_speed, y_speed, x_accel, y_accel, map, surface
    super x, y, x_speed, y_speed, surface

    @cur_x_speed = 0
    @cur_y_speed = 0
    @x_accel = x_accel
    @y_accel = y_accel
    @moving_left = false
    @moving_right = false
    @map = map
    @state = CharacterState.new self
    @prev_state = nil
  end

  def move direction
    @state.move direction
  end

  def airborn?
    bl = @map[@rect.bottomleft[0]/@map.tile_size][@rect.bottomleft[1]/@map.tile_size]
    br = @map[@rect.bottomright[0]/@map.tile_size][@rect.bottomright[1]/@map.tile_size]
    unless (bl == nil && br == nil)
      unless bl == nil
        y = @y.floor(-1)
        self.set_y y
      end
      unless br == nil
        y = @y.floor(-1)
        self.set_y y
      end
      false
    else
      true
    end
  end

  def collision?
    #top left
    tl_x = @rect.topleft[0]/@map.tile_size
    tl_y = @rect.topleft[1]/@map.tile_size
    tl = @map[tl_x][tl_y]

    #top right
    tr_x = @rect.topright[0]/@map.tile_size
    tr_y = @rect.topright[1]/@map.tile_size
    tr = @map[tr_x][tr_y]

    #bottom left
    bl_x = @rect.bottomleft[0]/@map.tile_size
    bl_y = @rect.bottomleft[1]/@map.tile_size
    bl = @map[bl_x][bl_y]

    #bottom right
    br_x = @rect.bottomright[0]/@map.tile_size
    br_y = @rect.bottomright[1]/@map.tile_size
    br = @map[br_x][br_y]

    #mid left
    ml_x = @rect.midleft[0]/@map.tile_size
    ml_y = @rect.midleft[1]/@map.tile_size
    ml = @map[ml_x][ml_y]

    #mid right
    mr_x = @rect.midright[0]/@map.tile_size
    mr_y = @rect.midright[1]/@map.tile_size
    mr = @map[mr_x][mr_y]

    #collision top
    if @cur_y_speed < 0
      unless tl == nil
        y = @y.ceil(-1)
        self.set_y y
      end
      unless tr == nil
        y = @y.creil(-1)
        self.set_y y
      end
    end

    #collision left
    if @cur_x_speed < 0
      if tl != nil && ml != nil
        x = @x.ceil(-1)
        self.set_x x
      end
      if bl != nil && ml != nil
        x = @x.ceil(-1)
        self.set_x x
      end
    end

    if @cur_x_speed > 0
      if tr != nil && mr != nil
        x = @x.floor(-1)
        self.set_x x
      end
      if br != nil && mr != nil
        x = @x.floor(-1)
        self.set_x x
      end
    end
  end

  def update
    if @moving_left
      self.move(-1)
    end
    if @moving_right
      self.move(1)
    end

    unless @moving_left || @moving_right
      self.move 0
    end

    self.collision?

    if self.airborn?
      unless @state.is_a?(Airborn)
        @prev_state = @state
        @state = Airborn.new self
      end
    else
      unless @prev_state == nil
        @state = @prev_state
        @prev_state = nil
        @cur_y_speed = 0
      end
    end

    @state.update
  end

  def handle_event event
    case event
    when Rubygame::KeyDownEvent
      if event.key == K_LEFT
        @moving_left = true
      end
      if event.key == K_RIGHT
        @moving_right = true
      end
    when Rubygame::KeyUpEvent
      if event.key == K_LEFT
        @moving_left = false
      end
      if event.key == K_RIGHT
        @moving_right = false
      end
    end
    @state.handle_event event
  end

  def set_x x
    @rect.left = x
    @x = x
    @cur_x_speed = 0
  end
  
  def set_y y
    @rect.top = y
    @y = y
    @cur_y_speed = 0
  end

end

class BananaMan < Character

  def initialize x, y, map
    surface = Rubygame::Surface.new [10, 20]
    surface.fill [255, 255, 0]

    super x, y, 7, 8, 2, 1, map, surface
  end
end

class CharacterState
  
  def initialize character
    @self = character
  end

  def move direction
    if direction < 0
      if @self.cur_x_speed > -@self.x_speed
        accel = self.accelerate @self.cur_x_speed, @self.x_speed, @self.x_accel
        @self.cur_x_speed -= accel
      end
    elsif direction > 0
      if @self.cur_x_speed < @self.x_speed
        accel = self.accelerate @self.cur_x_speed, @self.x_speed, @self.x_accel
        @self.cur_x_speed += accel
      end
    elsif @self.cur_x_speed > 0
      @self.cur_x_speed -= [@self.x_accel, @self.cur_x_speed.abs].min
    elsif @self.cur_x_speed < 0
      @self.cur_x_speed += [@self.x_accel, @self.cur_x_speed.abs].min
    end

    @self.x += @self.cur_x_speed
    @self.y += @self.cur_y_speed
    @self.rect.tl = @self.x, @self.y
  end

  def accelerate speed, max_speed, accel
    [(speed.abs - max_speed).abs + 1, accel].min
  end

  def jump
    # top left is (0, 0)
    @self.cur_y_speed = -20
  end

  def update
    @self.cur_y_speed = 0
  end

  def handle_event event
    case event
    when Rubygame::KeyDownEvent
      if event.key == K_SPACE
        self.jump
      end
    end
  end
end

class Airborn < CharacterState

  def initiate character
    super character

    @x_accel = 1
  end

  def fall
    # top left is (0,0)
    if @self.cur_y_speed < @self.y_speed
      @self.cur_y_speed += self.accelerate @self.cur_y_speed, @self.y_speed, @self.y_accel
    end
  end

  def jump
  end

  def update
    self.fall
  end
end
