class Map < Element

  attr_accessor :tile_size, :grid, :end_x, :end_y
  
  def initialize width, height
    @tile_size = 10
    @grid = Array.new(width) { Array.new(height) }
    @end_x = (@tile_size * width)
    @end_y = (@tile_size * height)

    @grid.each_with_index do |g, i|
      s = Rubygame::Surface.new [@tile_size, @tile_size]
      s.fill [255, 255, 255-i]
      e = Element.new i*@tile_size, (g.length-1)*@tile_size, s
      g[g.length-1] = e
      if i == 25
        s = Rubygame::Surface.new [@tile_size, @tile_size]
        s.fill [255, 255, 255-i]
        e = Element.new i*@tile_size, (g.length-2)*@tile_size, s
        g[g.length-2] = e
      end
    end
  end

  def draw x_offset, y_offset, screen
    @grid.each do |x|
      x.each do |y|
        unless y == nil
          y.draw x_offset, y_offset, screen
        end
      end
    end
  end

  def get x, y
    @grid[x][y]
  end

  def [] index
    @grid[index]
  end
end
