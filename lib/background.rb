require_relative 'element.rb'

class Background < Element

  def initialize width, height
    surface = Rubygame::Surface.new [width, height]

    super 0, 0, surface
  end
end
