include Rubygame
class Element
  include Sprites::Sprite

  attr_accessor :x, :y, :width, :height, :surface

  def initialize x, y, surface
    super()

    @surface = surface
    @rect = surface.make_rect
    @rect.tl = x, y
    @x, @y = @rect.tl

    @width = surface.width
    @height = surface.height
  end

  def update
  end

  def center_x w
    @x = w/2 - @width/2
  end

  def center_y h
    @y = h/2 - height/2
  end

  def center w, h
    center_x w
    center_y h
  end

  def draw screen, x_offset = 0, y_offset = 0
    @surface.blit screen, [@x-x_offset, @y-y_offset]
  end

  def handle_event event
  end
end
